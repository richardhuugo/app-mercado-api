import Grupo from "../models/Grupo"
import Categoria from "../models/Categoria"
import Produto from "../models/Produto"
 
 export const listarProdutos =async (req, res, next) => {
    let produtos = await Produto.find();
    return res.json(produtos)
 }
 export const registrarProduto = (req, res, next) => {
    const {codigo, produto, categoria_id} = req.body
    let novoProduto = new Produto({
        produto,
        categoria:categoria_id,
        codigo
    });
    novoProduto.save();
    return res.json({message:'Produto registrado com sucesso!'})
 }

 export const registrarCategoria = (req, res, next) => {
    const {categoria, grupo_id} = req.body

    let novaCategoria = new Categoria({
        grupo_id,
        categoria
    })
    novaCategoria.save();

    return res.json({message:'Categoria registrada com sucesso!'})
 }
 export const listarCategoria = async (req, res, next) => {
    let categoria = await Categoria.find();
    return res.json(categoria)
 }

 export const registrarGrupo = async (req, res, next) => {
    const {grupo} = req.body

    const novoGrupo = new Grupo({grupo});
    novoGrupo.save();

    return res.json({message:'Grupo registrado com sucesso!'})
 }
 export const listarGrupo = (req, res, next) => {
    let grupos = Grupo.find();
    return res.json(grupos)
 }