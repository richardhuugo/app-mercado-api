import Usuario from "../models/Usuario";
import Cliente from "../models/Cliente";
import { gerarPassword } from "../config/utils";
import * as bcrypt from 'bcrypt';
import Empresa from "../models/Empresa";
import {CLIENTE, FUNCIONARIO, EMPRESA} from '../config/constants'
import * as jwt from 'jsonwebtoken';
import Funcionario from "../models/Funcionario";

export const cadastrarClienteId = (req, res, next) => {
 
    const clienteSave = new Cliente(req.body);
    clienteSave.save();

    // enviar e-mail de cadastro realizado com sucesso

    // enviar sms

    return res.status(200).send({message:'Cadastro realizado com sucesso!'})
}
export const cadastrarCliente = async (req, res, next) => {

        const salt = bcrypt.genSaltSync()
        req.body.senha = bcrypt.hashSync(req.body.senha, salt)
    
        //realizar cadastro
        const usuario = new Usuario(req.body);
    
        const cliente = new Cliente({usuario_id:usuario._id});
            
        
        usuario.save( )
        cliente.save();

        // enviar e-mail de cadastro realizado com sucesso

        // enviar sms
        return res.status(200).send({message:'Cadastro realizado com sucesso!'})
}
export const cadastrarFuncionarioId = (req, res, next) => {
 
    const clienteSave = new Cliente(req.body);
    clienteSave.save();

    // enviar e-mail de cadastro realizado com sucesso

    // enviar sms

    return res.status(200).send({message:'Cadastro realizado com sucesso!'})
}
export const cadastrarFuncionario = (req, res, next) => {
    
    const salt = bcrypt.genSaltSync()
    req.body.senha = bcrypt.hashSync(req.body.senha, salt)

    //realizar cadastro
    const usuario = new Usuario(req.body);

    const funcionario = new Funcionario({
        usuario_id:usuario._id
    });
    usuario.save()
    funcionario.save();

    // enviar e-mail de cadastro realizado com sucesso

    // enviar sms
    return res.status(200).send({message:'Cadastro realizado com sucesso!'})
 
}
export const cadastrarEmpresa = (req, res, next) => {
    
    const salt = bcrypt.genSaltSync()
    req.body.senha = bcrypt.hashSync(req.body.senha, salt)

    const empresa = new Empresa(req.body);
    empresa.save((err) => {
        if(err)
            return res.status(400).send(err)
        
        return res.status(200).send({message:'Empresa cadastrada com sucesso!'})
    });


}

export const cadastrarFilial = (req, res, next) => {

}
const verificarPerfil = (data) => {
    let dataArrat = [];
    data.map(info => {
        if(info.cliente !=null){
            dataArrat.push({

            })
        }
    })
}
export const login = async (req, res, next) => {
   
    const {documento, senha} = req.body

    let user = await Usuario.findOne({documento})
     
    if(!bcrypt.compareSync(senha, user.senha))
        return res.status(400).json({message:'Usuario ou senha inválidos'})
 
    user = user.toObject();

    const cliente = await Cliente
    .findOne({usuario_id:user._id})    
    .populate({  path: 'usuario_id'}).exec()

    const funcionario = await Funcionario
    .findOne({usuario_id:user._id}).exec() 

   
    user.cliente = cliente;
    user.funcionario = funcionario
    user.empresa = null;
     const token = jwt.sign(user, process.env.KEY_SERVER.trim(), {
        expiresIn: "8h"
     })
    
    
    return res.status(200).send({ nome: user.nome,  access_token:token})

}
export const loginEmpresa = async (req, res, next) => {
    const {email, senha} = req.body

    let empresa = await Empresa.findOne({email}).exec()
     
    if(!bcrypt.compareSync(senha, empresa.senha))
        return res.status(400).json({message:'Usuario ou senha inválidos'})
 
    empresa = empresa.toObject();
 
    empresa.empresa = []
    const token = jwt.sign(empresa, process.env.KEY_SERVER.trim(), {
        expiresIn: "8h"
    })
        
        
        return res.status(200).send({ nome: empresa.razao_social,  access_token:token})
    
}

export const recuperarsenha = (req, res, next) => {
    const {email} = req.body
}
export const recuperarSenhaEmpresa = async(req, res, next) => {
    const {email} = req.body

    const found = Empresa.findOne({email_corporativo:email})

    

}

