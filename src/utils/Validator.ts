import {check,body, validationResult} from 'express-validator/check'
import { CADASTRAR_CLIENTE_ROTA, CADASTRAR_FUNC_ROTA, CADASTRAR_EMP_ROTA, CADASTRAR_CLIENTE_ROTA_ID, CADASTRAR_FUNC_ROTA_ID, LOGIN_EMPRESA, LOGIN_ADM, CADASTRAR_PRODUTO, CADASTRAR_GRUPO, CADASTRAR_CATEGORIA, LOGIN, CADASTRAR_PRODUTO_EMPRESA } from '../config/constants';
import Usuario from '../models/Usuario';
import * as mongoose from 'mongoose';
import Funcionario from '../models/Funcionario';
import Cliente from '../models/Cliente';
import Empresa from '../models/Empresa';
import usuario from '../config/routes/usuario';
 
export const validator =    (method) => {
 
    switch (method) {
        case CADASTRAR_PRODUTO_EMPRESA:{
          return [
            
          ]
        }
        case CADASTRAR_CLIENTE_ROTA: {
          return similarCadastro( )
        }
        case CADASTRAR_FUNC_ROTA: {
          return similarCadastro()
        }
        case CADASTRAR_CLIENTE_ROTA_ID: {
          return [
            body('usuario_id').isMongoId().not().isEmpty().withMessage('Informe um id valido').custom(value => {
         
              return  Cliente.findOne({usuario_id:value}).then(docs => {
                if(docs    ){
                  return Promise.reject('O id informado já se encontra cadastrado como cliente no sistema')
                }
              })
            })
          ]
        }
        case CADASTRAR_FUNC_ROTA_ID: {
          return [
            body('usuario_id').isMongoId().not().isEmpty().withMessage('Informe um id valido').custom(value => {
              return  Cliente.findOne({usuario_id:value}).then(docs => {
                if(docs    ){
                  return Promise.reject('O id informado já se encontra cadastrado como cliente no sistema')
                }
              })
            }),
          ]
        }
        case CADASTRAR_EMP_ROTA: {
          return [
            body('razao_social').not().isEmpty().withMessage('Infome a razão social'),
            body('nome_fantasia').not().isEmpty().withMessage('Infome o nome fantasia'),
            body('cnpj').not().isEmpty().withMessage('Infome o cnpj').custom(value => {
                return Empresa.findOne({cnpj:value}).then((data) => {
                  if(data){
                    return Promise.reject('A empresa já está cadastrada no sistema.')
                  }
                })
            }),
            body('email').isEmail().withMessage('Infome o e-mail'),
            body('senha').not().isEmpty().withMessage('Infome a senha'),
            body('telefone').not().isEmpty().withMessage('Infome o telefone'),
            body('celular').optional().not().isEmpty().withMessage('Infome o celular'),
            
          ]
        }
    
        case LOGIN: {
         return [
          body('documento').not().isEmpty().withMessage('Informe um documento válido').custom(value => {
            return Usuario.findOne({documento:value}).then(data => {
              
              if(data ==null){
                return Promise.reject('Usuario ou senha invalidos, tente novamente')
              }
            })
          }),
          body('senha').not().isEmpty().withMessage('Informe a senha')
         ]
        }
        case LOGIN_EMPRESA: {
         return [
          body('email').isEmail().withMessage('Informe um e-mail válido').custom(value => {
            return Empresa.findOne({email:value}).then(data => {
                if(!data){
                  return Promise.reject('usuario ou senha invalidos, tente novamente')
                } 
            })
          }),
          body('senha').not().isEmpty().withMessage('Informe a senha')
         ]
        }
        case LOGIN_ADM: {

        }
        case CADASTRAR_PRODUTO:{
          return [
            body('codigo'),
            body('produto'),
            body('preco')
          ]
        }
        case CADASTRAR_GRUPO:{

        }
        case CADASTRAR_CATEGORIA:{
          
        }
        // case 'registrar-cliente':  {return similar();}          
        // case 'registrar-funcionario': {return similar();}    
        // case 'registrar-empresa': {return similar();}     
        
        default:
          return []
      }
}
const similarCadastro = ( ) => {
  return [
    body('nome').not().isEmpty().withMessage('Informe o nome'),
    body('sobrenome').not().isEmpty().withMessage('Informe o sobrenome'),
    body('documento').not().isEmpty().withMessage('Informe o documento').custom((value ) => {
         return  Usuario.findOne({documento:value}).then(docs => {
            if(docs    ){
              return Promise.reject('Documento já cadastrado no sistema');
            }
          })
    }),            
    body('email').isEmail().withMessage('Informe um email correto').custom((value ) => {
      return Usuario.findOne({email:value}).then(docs => {
        if(docs ){
          return Promise.reject('E-mail já cadastrado no sistema');
        }
      })
    }),
    body('contato').not().isEmpty().withMessage('Informe o celular').custom((value ) => {
      return Usuario.findOne({contato:value}).then(docs => {
        if(docs){
          return Promise.reject('Celular já cadastrado no sistema');
        }
      })
    }),
    body('segundo_contato').optional(),
    body('senha').not().isEmpty().withMessage('Informe a senha')
  ]
}
const similarLogin = () => {
  return [
    
  ]
}

export const verify = (req, res, next) => {
    req.getValidationResult()    
    .then(() => {           
     const errors = validationResult(req);         
     if (!errors.isEmpty()) 
       return res.status(422).json({ errors: errors.array() });
    
       next();                        
    })    
    .catch(() => {
      const errors = validationResult(req);     
     return  res.json(errors.array())
    })
}