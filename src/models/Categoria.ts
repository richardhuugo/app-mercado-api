import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Categoria = new Schema({
    grupo_id: { type: Schema.Types.ObjectId, ref: 'Grupo'},
    categoria: { type: String, required:true}  
});

export default mongoose.model('Categoria', Categoria);
