import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let KitProduto = new Schema({
    kit_id: { type: Schema.Types.ObjectId,ref: 'Kit'},
    empresa_produto_id: { type: Schema.Types.ObjectId,ref: 'Kit'},
    
}, {
    timestamps: true
});

export default mongoose.model('KitProduto', KitProduto);
