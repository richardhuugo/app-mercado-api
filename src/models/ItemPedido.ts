import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let ItemPedido = new Schema({
    pedido_id: { type: Schema.Types.ObjectId, ref: 'Pedido'},
    empresa_produto_id: { type: Schema.Types.ObjectId, ref: 'EmpresaProduto'},
    kit_id: { type: Schema.Types.ObjectId,ref: 'Kit'},
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});

export default mongoose.model('ItemPedido', ItemPedido);
