import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Produto = new Schema({
    categoria:  { type: Schema.Types.ObjectId, ref: 'Categoria'},
    codigo: { type: String, required: true },
    produto: { type: String, required: true },
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('Produto', Produto);
