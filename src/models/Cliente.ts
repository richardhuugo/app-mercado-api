import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Cliente = new Schema({
    usuario_id: { type: Schema.Types.ObjectId, ref: 'Usuario'} 
});

export default mongoose.model('Cliente', Cliente);
