import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let PedidoStatus = new Schema({
    pedido_id:  { type: Schema.Types.ObjectId, ref: 'Pedido'},
    status_id: { type: Schema.Types.ObjectId, ref: 'Status'}
    
} );

export default mongoose.model('PedidoStatus', PedidoStatus);
