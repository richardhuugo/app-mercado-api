import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Status = new Schema({
   
    status: { type: String, required: true}
    
} );

export default mongoose.model('Status', Status);
