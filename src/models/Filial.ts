import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Filial = new Schema({
    endereco_id: { type: Schema.Types.ObjectId, ref: 'Endereco'},
    empresa_id: { type: Schema.Types.ObjectId, ref: 'Empresa'},
    
}, {
    timestamps: true
});

export default mongoose.model('Filial', Filial);
