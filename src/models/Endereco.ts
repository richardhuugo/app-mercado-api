import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Endereco = new Schema({
   
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('Endereco', Endereco);
