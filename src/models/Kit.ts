import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Kit = new Schema({
    descricao: { type: String,required:true},
    valor: { type: String,required:true},
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});

export default mongoose.model('Kit', Kit);
