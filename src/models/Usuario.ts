import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Usuario = new Schema({
    nome: { type: String, required: true },
    sobrenome: { type: String, required: true },
    email: { type: String, required: true, index: true,unique:true },
    senha: { type: String, required: true },
    documento: {type: String, required:true, index: true, unique:true},
    contato: {type: String, required:true, index: true, unique:true},
    segundo_contato: {type: String, required:false},
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('Usuario', Usuario);
