import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Pagamento = new Schema({
    pedido_id:{ type: Schema.Types.ObjectId, ref: 'Pedido'},
    funcionario_id:{ type: Schema.Types.ObjectId, ref: 'Funcionario'},
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});

export default mongoose.model('Pagamento', Pagamento);
