import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Carrinho = new Schema({
    usuario_id: { type: Schema.Types.ObjectId, ref: 'Usuario'},
    produtos: [{ type: Schema.Types.ObjectId, ref: 'EmpresaProduto'}],
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('Carrinho', Carrinho);
