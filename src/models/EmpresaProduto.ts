import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let EmpresaProduto = new Schema({
    empresa_id: { type: Schema.Types.ObjectId, ref: 'Empresa'},
    filial_id: { type: Schema.Types.ObjectId, ref: 'Filial'},
    produto_id: { type: Schema.Types.ObjectId, ref: 'Produto'},
    preco: { type: String, required: true },
    quantidade:{ type: String, required: true },
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('EmpresaProduto', EmpresaProduto);
