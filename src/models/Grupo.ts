import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Grupo = new Schema({
    grupo: { type: String, required: true }
});

export default mongoose.model('Grupo', Grupo);
