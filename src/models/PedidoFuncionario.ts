import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let PedidoFuncionario = new Schema({
    funcionario_id:  { type: Schema.Types.ObjectId, ref: 'Funcionario'},
    pedido_id: { type: Schema.Types.ObjectId, ref: 'Pedido'},
    token_pedido: {type:String, required:true}
    
} );

export default mongoose.model('PedidoFuncionario', PedidoFuncionario);
