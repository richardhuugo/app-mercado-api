import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Empresa = new Schema({
    razao_social:{type:String, required:true},
    nome_fantasia:{type:String, required:true},
    cnpj:{type:String, required:true, unique:true},
    email:{type:String, required:true, unique:true},
    endereco_id: { type: Schema.Types.ObjectId, ref: 'Endereco'},
    senha:{type:String, required:true},
    telefone:{type:String, required:true},
    celular:{type:String, required:false},
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('Empresa', Empresa);
