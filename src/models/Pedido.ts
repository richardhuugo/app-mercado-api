import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Pedido = new Schema({
    cliente_id: { type: Schema.Types.ObjectId, ref: 'Cliente'},
    data_entrega:{type:String, required:true}, 
    horario_entrega:{type:String, required:true}, 
    token_pedido:{type:String, required:true}, 
    cliente_endereco: { type: Schema.Types.ObjectId, ref: 'ClienteEndereco'}, 
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});

export default mongoose.model('Pedido', Pedido);
