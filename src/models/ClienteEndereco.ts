import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let ClienteEndereco = new Schema({
    endereco_id: { type: Schema.Types.ObjectId, ref: 'Endereco'},
    cliente_id: [{ type: Schema.Types.ObjectId, ref: 'Cliente'}],
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('ClienteEndereco', ClienteEndereco);
