import {server, app} from './config/server';
require('dotenv').config()
import './config/database';
import {index} from './config/routes/index';
index(app)
export default {server}

