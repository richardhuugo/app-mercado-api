import {GraphQLResolveInfo} from 'graphql';
import User from '../../../models/Usuario';

export const userResolvers = {

    Query: {
        users: (parent, {first = 2, offset=0 }, context, info:GraphQLResolveInfo) => {

            return  User.find()
            .limit(first)
            .skip(offset)
            .sort({
                tipo: 'asc'
            })
            .then( sucesso  => {
                return sucesso
            })          
          
        }
    },
    Mutation: {
        createUser:   (parent, args, context, info:GraphQLResolveInfo) => {
              
                    
            return  User.findOne({email:args.input.email} ).then(sucesso => {
                        if(sucesso){
                            return sucesso
                        }
                                                    
                        return new User({
                            nome:args.input.nome,
                            sobrenome:args.input.sobrenome,
                            email:args.input.email,
                            senha:'12345',
                            tp_user: args.tipo
                        }).save().then( up => {
                            return up
                        }) 
                    }) 
 
        }
    }
}