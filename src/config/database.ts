const mongoose = require('mongoose')
mongoose.set('debug', true);

mongoose.Promise = global.Promise
const config = {
    autoIndex: false,
    useNewUrlParser: true,
  };
   console.log('mongodb://'+process.env.USER_DB+':'+process.env.PASSWORDDB+'@'+process.env.HOST+':'+process.env.PORT+'/'+process.env.DATABASE )
export default  mongoose.connect('mongodb://'+process.env.USER_DB+':'+process.env.PASSWORDDB+'@'+process.env.HOST+':'+process.env.PORT+'/'+process.env.DATABASE ,config)

mongoose.Error.messages.general.required = "O atributo '{PATH}' é obrigatório."
mongoose.Error.messages.Number.min = 
    "O '{VALUE}' informado é menor que o limite mínimo de '{MIN}'."
    
mongoose.Error.messages.Number.max = 
    "O '{VALUE}' informado é maior que o limite máximo de '{MAX}'."
mongoose.Error.messages.String.enum = 
    "'{VALUE}' não é válido para o atributo '{PATH}'."