import * as express from 'express';
import * as graphsqlHTTP from 'express-graphql';
import * as bodyParser from 'body-parser'
import * as queryParser from 'express-query-int'
import cors from './cors';
import schema from '../graphql/schema';
import * as expressValidator from 'express-validator';
class Application {
    public express: express.Application;
    
    constructor() {
        this.express = express();
        this.middleware();
    }

    private middleware(): void {
        this.express.use(bodyParser.urlencoded({ extended: true }))
        this.express.use(bodyParser.json())
        this.express.use(cors)
        this.express.use(queryParser())
        this.express.use(expressValidator())
   
       /** this.express.use('/graphql', graphsqlHTTP({
            schema:schema,
            rootValue: global,
            graphiql: process.env.NODE_ENV.trim() === 'development'
        })) */
    }
}

export default new Application().express