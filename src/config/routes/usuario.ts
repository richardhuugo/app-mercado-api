import {validator, verify}  from '../../utils/Validator'
import { cadastrarCliente, cadastrarFuncionario, cadastrarEmpresa, cadastrarFuncionarioId, cadastrarClienteId, recuperarsenha, loginEmpresa, login } from '../../Controllers/AuthController';
import { CADASTRAR_CLIENTE_ROTA, CADASTRAR_FUNC_ROTA, CADASTRAR_EMP_ROTA, CADASTRAR_FUNC_ROTA_ID, CADASTRAR_CLIENTE_ROTA_ID, LOGIN_EMPRESA,   RECUPERAR_SENHA, LOGIN } from '../constants';
import { registrarProduto, listarProdutos, listarGrupo, listarCategoria } from "../../Controllers/ProdutoController";
import { CADASTRAR_PRODUTO, CADASTRAR_CATEGORIA, CADASTRAR_GRUPO } from "../constants";

const usuario = (openApi) => {    

    openApi.post('/registrar-cliente',validator(CADASTRAR_CLIENTE_ROTA),verify,cadastrarCliente);
    openApi.post('/registrar-cliente-id',validator(CADASTRAR_CLIENTE_ROTA_ID),verify,cadastrarClienteId);

    openApi.post('/registrar-funcionario',validator(CADASTRAR_FUNC_ROTA),verify,cadastrarFuncionario);
    openApi.post('/registrar-funcionario-id',validator(CADASTRAR_FUNC_ROTA_ID),verify,cadastrarFuncionarioId);
    
    openApi.post('/registrar-empresa',validator(CADASTRAR_EMP_ROTA),verify,cadastrarEmpresa);

    openApi.post('/login-empresa',validator(LOGIN_EMPRESA),verify,loginEmpresa);
    openApi.post('/login',validator(LOGIN),verify,login);
      
    openApi.post('/recuperar-senha',validator(RECUPERAR_SENHA),verify,recuperarsenha);
 

    // cadastrar grupos categorias e produtos

    openApi.get('/listar-produtos', listarProdutos);
    openApi.get('/listar-grupos',listarGrupo);
    openApi.get('/listar-categorias', listarCategoria);
    
    openApi.post('registrar-produto',validator(CADASTRAR_PRODUTO), verify, registrarProduto);
    openApi.post('registrar-categoria',validator(CADASTRAR_CATEGORIA), verify, registrarProduto);
    openApi.post('registrar-grupo',validator(CADASTRAR_GRUPO), verify, registrarProduto);
}
export default usuario
