import * as  express from 'express';
import usuario from './usuario';
import administrador from './administrador';
import cliente from './cliente';
import ClienteAuth from '../auth/ClienteAuth';
import EmpresaAuth from '../auth/EmpresaAuth';
import empresa from './empresa';
var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');
export const index = (server) => {
    const openApi = express.Router()
    server.use('/v1',openApi) 

    server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    usuario(openApi);

    const clienteApi = express.Router()
    server.use('/v1/cliente',clienteApi) 
    clienteApi.use(ClienteAuth)
    cliente(openApi);

    const empresaApi = express.Router();
    server.use('/v1/empresa',empresaApi)
    empresaApi.use(EmpresaAuth)
    empresa(empresaApi)
    // const empresaApi= express.Router()
  
    // server.use('/v1/empresa',empresaApi) 
    // const funcionarioApi= express.Router()
    // server.use('/v1/funcionario',funcionarioApi) 
    // const alunoApi= express.Router()
    // server.use('/v1/aluno',alunoApi) 
    // const administradorApi= express.Router();
    // server.use('/v1/admin',administradorApi) 

  
    // administrador(administradorApi);
    
    // empresaApi.use(EmpresaAuth);
    // empresa(empresaApi)
}