import { verify, validator } from "../../utils/Validator";
import { registrarProduto, listarProdutos, listarGrupo, listarCategoria } from "../../Controllers/ProdutoController";
import { CADASTRAR_PRODUTO, CADASTRAR_CATEGORIA, CADASTRAR_GRUPO } from "../constants";

// import { registrarTipo } from "../../Controllers/AdministradorController";
// import { verify, validator } from "../../utils/Validator";
// import { REGISTRAR_TIPO_USER } from "../constants";

const administrador = (api) => {
    // api.post('/registrar-tipo-usuario',validator(REGISTRAR_TIPO_USER),verify, registrarTipo);
    api.get('/listar-produtos', listarProdutos);
    api.get('/listar-grupos',listarGrupo);
    api.get('/listar-categorias', listarCategoria);
    
    api.post('registrar-produto',validator(CADASTRAR_PRODUTO), verify, registrarProduto);
    api.post('registrar-categoria',validator(CADASTRAR_CATEGORIA), verify, registrarProduto);
    api.post('registrar-grupo',validator(CADASTRAR_GRUPO), verify, registrarProduto);
    
}


export default administrador;