import { ALUNO, ADMIN } from "../constants";
const jwt = require('jsonwebtoken')

const AdministradorAuth =  (req, res, next) => {

        const token =   req.headers['authorization']        
        if (!token) 
            return res.status(403).send({ errors: ['No token provided.'] })
        
        if(token.search("Bearer") !=0 )
            return res.status(403).send({ errors: ['No token provided.'] })
            
        const divid = token.split(" ");
        
        jwt.verify(divid[1],  process.env.KEY_SERVER.trim(), function (err, decoded) {
            if (err) {
                return res.status(403).send({
                    errors: ['Failed to authenticate token.']
                })
            } else {
                if( !decoded.administrador )
                    return res.status(403).send({ errors: ['Failed to authenticate token.'] })
                                  
                req.decoded = decoded
                next()
            }
        })
   
}

export default AdministradorAuth;